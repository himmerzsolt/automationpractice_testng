package com.automationpractice.pageobjects.locators;

import com.automationpractice.pageobjects.PageObject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
public class TopMenuLocators extends PageObject {
	
	@FindBy(css = "#block_top_menu > ul > li:nth-child(1) > a")
	private WebElement womenCategorySelector;
	
	@FindBy(css = "#block_top_menu > ul > li:nth-child(2) > a")
	private WebElement dressCategorySelector;
	
	@FindBy(css = "#block_top_menu > ul > li:nth-child(3) > a")
	private WebElement tshirtsCategorySelector;
	
	@FindBy(css = "#subcategories > ul > li:nth-child(1) > div.subcategory-image > a")
	private WebElement topsSelector;
	
	@FindBy(css = "#subcategories > ul > li:nth-child(2) > div.subcategory-image > a")
	private WebElement dressesSelector;
	
	@FindBy(css = "#subcategories > ul > li:nth-child(1) > div.subcategory-image > a")
	private WebElement casualDressesSelector;
	
	@FindBy(css = "#subcategories > ul > li:nth-child(2) > div.subcategory-image > a")
	private WebElement eveningDressesSelector;
	
	@FindBy(css = "#subcategories > ul > li:nth-child(3) > div.subcategory-image > a")
	private WebElement summerDressesSelector;
	
	public TopMenuLocators (WebDriver driver){
		super(driver);
	}
	
	
	public WebElement getWomenCategorySelector () {
		return womenCategorySelector;
	}
	
	
	public WebElement getDressCategorySelector () {
		return dressCategorySelector;
	}
	
	
	public WebElement getTshirtsCategorySelector () {
		return tshirtsCategorySelector;
	}
	
	
	public WebElement getTopsSelector () {
		return topsSelector;
	}
	
	
	public WebElement getDressesSelector () {
		return dressesSelector;
	}
	
	
	public WebElement getCasualDressesSelector () {
		return casualDressesSelector;
	}
	
	
	public WebElement getEveningDressesSelector () {
		return eveningDressesSelector;
	}
	
	
	public WebElement getSummerDressesSelector () {
		return summerDressesSelector;
	}
}
