package com.automationpractice.pageobjects.locators;

import com.automationpractice.pageobjects.PageObject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
public class LandingPageLocators extends PageObject {
	
	@FindBy(css = "#columns > div.breadcrumb.clearfix > span.navigation_page")
	private WebElement myAccount;
	
	
	public LandingPageLocators (WebDriver driver) {
		super(driver);
	}
	
	
	public WebElement getMyAccount () {
		return myAccount;
	}
}
