package com.automationpractice.pageobjects.locators;

import com.automationpractice.pageobjects.PageObject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;
public class ResultsLocator extends PageObject {
	
	@FindBy(css = "#center_column > h1 > span")
	private WebElement resultCounterText;
	
	//xpath find all child li with div tag inside
//        return driver.findElements(By.xpath("//*[@id='center_column']/ul//child::li[div]//*[@class='product-container']//*[@class='left-block']/div/a[1]"));
	@FindBy(css = "#center_column > ul > li > div > div.left-block > div.product-image-container > a:nth-child(1)")
	private List<WebElement> searchResults;
	
	@FindBy(css = "#selectProductSort > option:nth-child(2)")
	private WebElement selectProductSortByPriceAsc;
	
	@FindBy(css = "#center_column > ul > li > div > div.right-block > div.content_price > span.price")
	private List<WebElement> searchResultPrices;
	
	
	public ResultsLocator (WebDriver driver) {
		super(driver);
	}
	
	
	public WebElement getResultCounterText () {
		return resultCounterText;
	}
	
	
	public List<WebElement> getSearchResults () {
		return searchResults;
	}
	
	
	public WebElement getSelectProductSortByPriceAsc () {
		return selectProductSortByPriceAsc;
	}
	
	
	public List<WebElement> getSearchResultPrices () {
		return searchResultPrices;
	}
}
