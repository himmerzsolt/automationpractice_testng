package com.automationpractice.pageobjects.locators;

import com.automationpractice.pageobjects.PageObject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPageLocators extends PageObject {
	
	@FindBy(css = "#email")
	private WebElement inputEmailAddress;
	
	@FindBy(css = "#passwd")
	private WebElement inputPassword;
	
	@FindBy(css = "#SubmitLogin > span > i")
	private WebElement submitLogin;
	
	@FindBy(css = "#center_column > div.alert.alert-danger > ol > li")
	private WebElement errorMessage;
	
	
	public LoginPageLocators (WebDriver driver) {
		super(driver);
	}
	
	
	public WebElement getInputEmailAddress () {
		return inputEmailAddress;
	}
	
	
	public WebElement getInputPassword () {
		return inputPassword;
	}
	
	
	public WebElement getSubmitLogin () {
		return submitLogin;
	}
	
	
	public WebElement getErrorMessage () {
		return errorMessage;
	}
}
