package com.automationpractice.pageobjects.locators;

import com.automationpractice.pageobjects.PageObject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
public class BasePageLocators extends PageObject {
	
	@FindBy(css = "#header > div.nav > div > div > nav > div.header_user_info > a")
	private WebElement signInButton;
	
	public BasePageLocators (WebDriver driver){
		super(driver);
	}
	
	
	public WebElement getSignInButton () {
		return signInButton;
	}
}
