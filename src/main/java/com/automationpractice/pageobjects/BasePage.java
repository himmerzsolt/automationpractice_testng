package com.automationpractice.pageobjects;

import com.automationpractice.pageobjects.locators.BasePageLocators;
import org.openqa.selenium.WebDriver;

public class BasePage extends PageObject {
	
	private final String BASE_URL = "http://automationpractice.com/index.php";
	
	private BasePageLocators basePageLocators;
	
	
	public BasePage (WebDriver driver) {
		super(driver);
		basePageLocators = new BasePageLocators(driver);
	}
	
	
	public void clickOnSignIn () {
		basePageLocators.getSignInButton().click();
	}
	
	
	public void init () {
		this.driver.get(this.BASE_URL);
		driver.manage().window().maximize();
	}
}
