package com.automationpractice.pageobjects;

import com.automationpractice.util.VisibleAjaxElementLocatorFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

//Every pageobject is supposed to extend  this Class
public class PageObject {
	WebDriver driver;
	
	
	public PageObject (WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(new VisibleAjaxElementLocatorFactory(driver, 2), this);
	}
	
	
	public void enterFullScreen () {
		driver.manage().window().maximize();
	}
	
	
	public void tearDown () {
		driver.quit();
	}
}
