package com.automationpractice.pageobjects.components;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
public class Button {
	private final WebDriver driver;
	
	private String cssSelector;
	
	
	public Button (WebDriver driver, String cssSelector) {
		this.driver = driver;
		this.cssSelector = cssSelector;
	}
	
	
	public void click () {
		final WebElement button = driver.findElement(By.cssSelector(cssSelector));
		button.click();
	}
}
