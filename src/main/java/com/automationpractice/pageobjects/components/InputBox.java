package com.automationpractice.pageobjects.components;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
public class InputBox {
	private final WebDriver driver;
	
	private String cssSelector;
	
	private WebElement inputField;
	
	
	public InputBox (WebDriver driver, String cssSelector) {
		this.cssSelector = cssSelector;
		this.driver = driver;
	}
	
	
	public void typeText (String text) {
		WebElement inputField = driver.findElement(By.cssSelector(cssSelector));
		inputField.sendKeys(text);
	}
	
	
	public void deleteText () {
		WebElement inputField = driver.findElement(By.cssSelector(cssSelector));
		inputField.clear();
	}
}
