package com.automationpractice.pageobjects.components;

import com.automationpractice.pageobjects.locators.TopMenuLocators;
import org.openqa.selenium.WebDriver;
public class TopMenu {
	WebDriver driver;
	
	TopMenuLocators topMenuLocators;
	
	
	public TopMenu (WebDriver driver) {
		this.driver = driver;
		topMenuLocators = new TopMenuLocators(driver);
	}
	
	
	public void selectCategory (String category) {
		switch (category) {
			case "Dresses":
				topMenuLocators.getDressCategorySelector().click();
				break;
			case "Women":
				topMenuLocators.getWomenCategorySelector().click();
				break;
			case "T-shirt":
				topMenuLocators.getTshirtsCategorySelector().click();
				break;
		}
	}
	
	
	public void selectSubCategory (String subCategory) {
		
		switch (subCategory) {
			
			case "Casual":
				topMenuLocators.getCasualDressesSelector().click();
				break;
			case "Evening":
				topMenuLocators.getEveningDressesSelector().click();
				break;
			case "Summer":
				topMenuLocators.getSummerDressesSelector().click();
				break;
			case "Tops":
				topMenuLocators.getTopsSelector().click();
				break;
			case "Dresses":
				topMenuLocators.getDressesSelector().click();
				break;
			default:
				break;
		}
	}
}
