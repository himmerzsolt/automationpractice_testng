package com.automationpractice.pageobjects;

import com.automationpractice.pageobjects.locators.LandingPageLocators;
import org.openqa.selenium.WebDriver;


public class Landing extends BasePage {
	
	private final LandingPageLocators landingPageLocators;
	
	
	public Landing (WebDriver driver) {
		super(driver);
		landingPageLocators = new LandingPageLocators(driver);
	}
	
	
	public String checkMyAccountButton () {
		return landingPageLocators.getMyAccount().getText();
	}
}
