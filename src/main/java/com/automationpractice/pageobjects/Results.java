package com.automationpractice.pageobjects;

import com.automationpractice.pageobjects.locators.ResultsLocator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Results extends BasePage {
	ResultsLocator resultsLocator;
	
	
	public Results (WebDriver driver) {
		super(driver);
		resultsLocator = new ResultsLocator(driver);
	}
	
	
	public String getContentResultCounterText () {
		return resultsLocator.getResultCounterText().getText();
	}
	
	
	public List<WebElement> getSearchResults () {
		return resultsLocator.getSearchResults();
	}
	
	
	public List<Double> sortSearchResultPricesAscending () {
	
//		List<WebElement> list = resultsLocator.getSearchResultPrices();
		List<Double> doubleList = this.getSearchResultPrices();
//				new ArrayList<>();
//		for (WebElement webElement : list) {
//			doubleList.add(Double.parseDouble(webElement.getText().substring(1)));
//		}
		Collections.sort(doubleList);
		return doubleList;
	}
	
	
	public List<Double> getSearchResultPrices () {
		
		List<WebElement> list = resultsLocator.getSearchResultPrices();
		List<Double> doubleList = new ArrayList<>();
		for (WebElement webElement : list) {
			doubleList.add(Double.parseDouble(webElement.getText().substring(1)));
		}
		return doubleList;
	}
	
	
	public void clickOnSelectProductSortByPriceAsc () {
		resultsLocator.getSelectProductSortByPriceAsc().click();
	}
}
