package com.automationpractice.pageobjects;

import com.automationpractice.pageobjects.locators.LoginPageLocators;
import org.openqa.selenium.WebDriver;

public class Login extends BasePage {
	
	private final LoginPageLocators loginPageLocators;
	
	
	public Login (WebDriver driver) {
		super(driver);
		loginPageLocators = new LoginPageLocators(driver);
	}
	
	
	public void typeEmailAddress (String email) {
		loginPageLocators.getInputEmailAddress().sendKeys(email);
	}
	
	
	public void typePassword (String pw) {
		loginPageLocators.getInputPassword().sendKeys(pw);
	}
	
	
	public void clickOnSignInButton () {
		loginPageLocators.getSubmitLogin().click();
	}
	
	
	public String checkErrorMessage () {
		return loginPageLocators.getErrorMessage().getText();
	}
}
