package com.automationpractice.util;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.pagefactory.ElementLocator;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;

import java.lang.reflect.Field;

public class VisibleAjaxElementLocatorFactory implements ElementLocatorFactory {
	private final int timeOutInSeconds;
	
	private final WebDriver driver;
	
	
	public VisibleAjaxElementLocatorFactory (WebDriver driver, int timeOutInSeconds) {
		this.driver = driver;
		this.timeOutInSeconds = timeOutInSeconds;
	}
	
	
	@Override
	public ElementLocator createLocator (Field field) {
		return new VisibleAjaxElementLocator(driver, field, timeOutInSeconds);
	}
}
