package com.automationpractice.util;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.grid.selenium.GridLauncherV3;
import org.openqa.grid.shared.Stoppable;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;


public class Grid {
	
	static String ip4 = getIP4Address();
	
	private final GridLauncherV3 gridLauncherV3 = new GridLauncherV3();
	
	Stoppable nodeChrome;
	
	Stoppable nodeFirefox;
	
	Stoppable hub;
	
	
	public static String getIP4Address () {
		Inet4Address i4 = null;
		try {
			i4 = (Inet4Address) InetAddress.getLocalHost();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		return i4.getHostAddress();
	}
	
	
	public void startHub () {
		hub = gridLauncherV3.launch(new String[]{"-role", "hub", "-port", "4444"});
		registerFirefox();
		registerChrome();
	}
	
	
	public void registerChrome () {
		WebDriverManager.chromedriver().setup();
		
		nodeChrome = gridLauncherV3.launch(new String[]{"-role", "node", "-hub",
				"http://" + ip4 + ":4444/grid/register/", "-browser",
				"browserName=chrome", "-port", "5566"});
	}
	
	
	public void registerFirefox () {
		WebDriverManager.firefoxdriver().setup();
		nodeFirefox = gridLauncherV3.launch(new String[]{"-role", "node", "-hub",
				"http://" + ip4 + ":4444/grid/register/", "-browser",
				"browserName=firefox", "-port", "5555"});
	}
	
	
	public void stopGrid () {
		hub.stop();
		nodeChrome.stop();
		nodeFirefox.stop();
	}
}

