package com.automationpractice.util;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.pagefactory.AjaxElementLocator;

import java.lang.reflect.Field;
public class VisibleAjaxElementLocator extends AjaxElementLocator {
	public VisibleAjaxElementLocator (WebDriver driver, Field field, int timeOutInSeconds) {
		super(driver, field, timeOutInSeconds);
	}
	
	
	@Override
	protected boolean isElementUsable (WebElement element) {
		//if we have not found the element yet, return false, we cannot synchronize yet
		if (element == null) {
			return false;
		}
		// if not null, we found one, we have to make sure element is displayed
		return element.isDisplayed() && element.isEnabled();
	}
}
