package com.automationpractice.util;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

// When I use retry Analyzer in my tests , Failed tests are considered as skipped
// It's working fine in 6.9.4 and below but not working above 6.9.4
public class RetryAnalyzerImpl implements IRetryAnalyzer {
	int count = 0;
	
	
	@Override
	public boolean retry (ITestResult iTestResult) {
		
		if ((!iTestResult.isSuccess() && count < 2)) {
			count++;
			return true;
		}
		return false;
	}
}
