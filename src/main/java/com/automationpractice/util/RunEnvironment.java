package com.automationpractice.util;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;

public class RunEnvironment {
	
	private static WebDriver driver;
	
	
	public static WebDriver getWebDriver (String os, String browser, String host, String port) {
		
		
		Platform platform = Platform.fromString(os.toUpperCase());
		if (browser.equalsIgnoreCase("chrome")) {
			ChromeOptions chromeOptions = new ChromeOptions();
			chromeOptions.setCapability("platform", platform);
			try {
				driver = new RemoteWebDriver(new URL(host + port + "/wd/hub"), chromeOptions);
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
		} else if (browser.equalsIgnoreCase("firefox")) {
			FirefoxOptions firefoxOptions = new FirefoxOptions();
			firefoxOptions.setCapability("platform", platform);
			try {
				driver = new RemoteWebDriver(new URL(host + port + "/wd/hub"), firefoxOptions);
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
		} else {
			InternetExplorerOptions ieOption = new InternetExplorerOptions();
			ieOption.setCapability("platform", platform);
			try {
				driver = new RemoteWebDriver(new URL(host + port + "/wd/hub"), ieOption);
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
		}
		return driver;
	}
}





