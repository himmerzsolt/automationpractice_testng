package com.automationpractice.util;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbookFactory;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
public class ExcelReader {
	
	public static Object[][] getInvalidEmailAddresses () {
		XSSFWorkbook workbook = null;
		Object[][] emailArray = null;
		try {
			workbook = (XSSFWorkbook) XSSFWorkbookFactory.create(new File("D:\\automationpractice_testng\\Authentication.xlsx"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		Sheet firstSheetOfWorkBook = null;
		if (workbook != null) {
			firstSheetOfWorkBook = workbook.getSheetAt(0);
			
			//since excel file has a header, getLastRowNum is fine, as it will return index number of rows, therefore 2 in case of 3 rows in total
			//only one element needed in each array for the email address
			
			emailArray = new Object[firstSheetOfWorkBook.getLastRowNum()][1];
			int i = 0;
			Iterator<Row> iterator = firstSheetOfWorkBook.rowIterator();
			//skipping the header
			iterator.next();
			while (iterator.hasNext()) {
				Row row = iterator.next();
				//cell number 0 contains the email
				emailArray[i][0] = row.getCell(0).toString();
				i++;
			}
		} else System.out.println("Excel workbook does not contain any worksheet!");
		return emailArray;
	}
	
	
	public static Object[][] getInvalidPassword () {
		XSSFWorkbook workbook = null;
		Object[][] passwordArray = null;
		try {
			workbook = (XSSFWorkbook) XSSFWorkbookFactory.create(new File("D:\\automationpractice_testng\\Authentication.xlsx"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		Sheet firstSheetOfWorkBook = null;
		if (workbook != null) {
			firstSheetOfWorkBook = workbook.getSheetAt(0);
			
			//since excel file has a header, getLastRowNum is fine, as it will return index number of rows, therefore 2 in case of 3 rows in total
			//only one element needed in each array for the password
			
			passwordArray = new Object[firstSheetOfWorkBook.getLastRowNum()][1];
			int i = 0;
			Iterator<Row> iterator = firstSheetOfWorkBook.rowIterator();
			//skipping the header
			iterator.next();
			while (iterator.hasNext()) {
				Row row = iterator.next();
				//cell number 1 contains the password
				passwordArray[i][0] = row.getCell(1).toString();
				i++;
			}
		} else System.out.println("Excel workbook does not contain any worksheet!");
		return passwordArray;
	}
	
	
	public static Object[][] getCategories () {
		XSSFWorkbook workbook = null;
		Object[][] categoryArray = null;
		try {
			workbook = (XSSFWorkbook) XSSFWorkbookFactory.create(new File("D:\\automationpractice_testng\\TopMenuTestData.xlsx"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		Sheet firstSheetOfWorkBook = null;
		if (workbook != null) {
			firstSheetOfWorkBook = workbook.getSheetAt(0);
			
			//since excel file has a header, getLastRowNum is fine, as it will return index number of rows, therefore 2 in case of 3 rows in total
			//two element needed in each array for the category and sub-category
			
			categoryArray = new Object[firstSheetOfWorkBook.getLastRowNum()][2];
			int i = 0;
			Iterator<Row> iterator = firstSheetOfWorkBook.rowIterator();
			//skipping the header
			iterator.next();
			while (iterator.hasNext()) {
				Row row = iterator.next();
				//cell number 0 contains the category
				categoryArray[i][0] = row.getCell(0).toString();
				//cell number 1 contains the sub-category
				categoryArray[i][1] = row.getCell(1).toString();
				i++;
			}
		} else System.out.println("Excel workbook does not contain any worksheet!");
		return categoryArray;
	}
}
