package com.automationpractice.login;

import com.automationpractice.pageobjects.BasePage;
import com.automationpractice.pageobjects.Landing;
import com.automationpractice.pageobjects.Login;
import com.automationpractice.util.CookieRead;
import com.automationpractice.util.Grid;
import com.automationpractice.util.RetryAnalyzerImpl;
import com.automationpractice.util.RunEnvironment;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class SuccessfulLoginTest {
	
	private WebDriver driver;
	
	private BasePage basePage;
	
	private Login login;
	
	private Landing landing;
	
	private final String host = "http://" + Grid.getIP4Address();
	
	private final Grid grid = new Grid();
	
	
	@Parameters({"os", "browser", "port"})
	@BeforeClass(groups = {"setup"})
	public void setup (String os, String browser, String port) {
		driver = RunEnvironment.getWebDriver(os, browser, this.host, port);
		basePage = new BasePage(driver);
		login = new Login(driver);
		landing = new Landing(driver);
	}
	
	
	@Test(groups = {"smoke"}, retryAnalyzer = RetryAnalyzerImpl.class)
	public void successfulLogin () {
		basePage.init();
		basePage.clickOnSignIn();
		login.typeEmailAddress("himmerzsolt@gmail.com");
		login.typePassword("test123");
		login.clickOnSignInButton();
		Assert.assertEquals(landing.checkMyAccountButton(), "My account", "login was successful");
		CookieRead.getCookies(driver);
	}
	
	
	@AfterClass (groups = {"setup"})
	public void quitBrowser () {
		basePage.tearDown();
	}
}
