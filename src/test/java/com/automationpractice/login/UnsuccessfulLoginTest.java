package com.automationpractice.login;

import com.automationpractice.pageobjects.BasePage;
import com.automationpractice.pageobjects.Login;
import com.automationpractice.util.ExcelReader;
import com.automationpractice.util.Grid;
import com.automationpractice.util.RetryAnalyzerImpl;
import com.automationpractice.util.RunEnvironment;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.*;

public class UnsuccessfulLoginTest {
	
	private BasePage basePage;
	
	private Login login;
	
	private final String host = "http://" + Grid.getIP4Address();
	
	
	@DataProvider(name = "wrong_email_provider")
	public Object[][] wrongEmail () {
		return ExcelReader.getInvalidEmailAddresses();
	}
	
	
	@DataProvider(name = "wrong_pw_provider")
	public Object[][] wrongPassword () {
		return ExcelReader.getInvalidPassword();
	}
	
	
	@Parameters({"os", "browser", "port"})
	@BeforeClass(groups = {"setup"})
	public void setup (String os, String browser, String port) {
		WebDriver driver = RunEnvironment.getWebDriver(os, browser, this.host, port);
		basePage = new BasePage(driver);
		login = new Login(driver);
	}
	
	
	@Test(dataProvider = "wrong_email_provider", priority = 1, groups = {"regression"}, retryAnalyzer = RetryAnalyzerImpl.class)
	public void unsuccessfulLoginWithInvalidEmail (String email) {
		basePage.init();
		basePage.clickOnSignIn();
		login.typeEmailAddress(email);
		login.typePassword("test123");
		login.clickOnSignInButton();
		Assert.assertEquals(login.checkErrorMessage(), "Authentication failed.", "error message is not the expected one!");
	}
	
	
	@Test(dataProvider = "wrong_pw_provider", priority = 2, groups = {"regression"}, retryAnalyzer = RetryAnalyzerImpl.class)
	public void unsuccessfulLoginWithInvalidPassword (String password) {
		basePage.init();
		basePage.clickOnSignIn();
		login.typeEmailAddress("himmerzsolt@gmail.com");
		login.typePassword(password);
		login.clickOnSignInButton();
		Assert.assertEquals(login.checkErrorMessage(), "Authentication failed.", "error message is not the expected one!");
	}
	
	
	@Test(priority = 3, groups = {"regression"}, retryAnalyzer = RetryAnalyzerImpl.class)
	public void unsuccessfulLoginWithValidEmailAndEmptyPassword () {
		basePage.init();
		basePage.clickOnSignIn();
		login.typeEmailAddress("himmerzsolt@gmail.com");
		login.clickOnSignInButton();
		Assert.assertEquals(login.checkErrorMessage(), ("Password is required."), "error message is not the expected one!");
	}
	
	
	@Test(priority = 4, groups = {"regression"}, retryAnalyzer = RetryAnalyzerImpl.class)
	public void unsuccessfulLoginWithEmptyEmailAndValidPassword () {
		basePage.init();
		basePage.clickOnSignIn();
		login.typePassword("test123");
		login.clickOnSignInButton();
		Assert.assertEquals(login.checkErrorMessage(), ("An email address required."), "error message is not the expected one!");
	}
	
	
	@AfterClass(groups = {"setup"})
	public void quitBrowser () {
		basePage.tearDown();
	}
}
