package com.automationpractice.listeners;

import org.testng.*;
import org.testng.xml.XmlSuite;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class CustomisedReports implements IReporter {
	
	private static final String ROW_TEMPLATE = "<tr class=\"%s\"><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>";
	
	
	// xmlSuite:This is the list of suites that exist in the XML file for execution.
	// suites: This is an object which contains all the information about the test execution and suite related information.
	// This information may include the package names, class names, test method names, and the test execution results.
	// outputDirectory: It contains the path to the output folder where the generated reports will be available.
	//Interface defining a Test Suite: ISuite
	public void generateReport (List<XmlSuite> xmlSuites, List<ISuite> suites, String outputDirectory) {
		String reportTemplate = initReportTemplate();
		
		final String body = suites
				.stream()
//                 intermediate operation returns a stream
				.flatMap(suiteToResults())
//        Returns a Collector that concatenates the input elements into a String, in encounter order.
				.collect(Collectors.joining());
		
		//concatenating string using format
		saveReportTemplate(outputDirectory, reportTemplate.replaceFirst("</tbody>", String.format("%s</tbody>", body)));
	}
	
	
	//function descriptor: ISuite - > Stream of Strings
	private Function<ISuite, Stream<? extends String>> suiteToResults () {
		//ISuite interface's getResult method returns a Map <String, ISuiteResult>
		//Map interface's entrySet returns a Set view of the mappings in the Map
		//Returns a Set containing the Map.Entry values
		//Map.Entry Interface: A map entry (key-value pair).
		// The Map.entrySet method returns a collection-view of the map, whose elements are of this class
		return suite -> suite.getResults().entrySet()
				.stream()
				.flatMap(resultsToRows(suite));
	}
	
	
	//Function descriptor Map<String, ISuiteResult> --> Stream of Strings
	//Map.Entry returns A map entry (key-value pair), where key is the test name
	private Function<Map.Entry<String, ISuiteResult>, Stream<? extends String>> resultsToRows (ISuite suite) {
		//e is a Map type object and GetValue returns the value from the key-value pair. The type of the value is ISuiteResult
		//GetTestContext returns The testing context for the tests.
		//This class defines a test context which contains all the information for a given test run
		return e -> {
			ITestContext testContext = e.getValue().getTestContext();
			
			Set<ITestResult> failedTests = testContext
					.getFailedTests()
					.getAllResults();
			Set<ITestResult> passedTests = testContext
					.getPassedTests()
					.getAllResults();
			Set<ITestResult> skippedTests = testContext
					.getSkippedTests()
					.getAllResults();
			
			String suiteName = suite.getName();
			
			
			//e.getKey() is the testName
			//A map where the key is the group and the value is a list of methods used by this group.
			
			
			List<ITestNGMethod> allTestMethods = suite.getMethodsByGroups().values().stream().flatMap(list -> list.stream()).collect(Collectors.toList());
			System.out.println(allTestMethods.get(0).getGroups()[0] + " " + allTestMethods.get(0).getMethodName());
			
			
			return Stream
					//Returns a sequential ordered stream whose elements are the specified values
					.of(failedTests, passedTests, skippedTests)
					.flatMap(results -> generateReportRows(e.getKey(), suiteName, allTestMethods, results).stream());
		};
	}
	
	
	private List<String> generateReportRows (String testName, String suiteName, List<ITestNGMethod> allTestMethods, Set<ITestResult> allTestResults) {
		return allTestResults.stream()
//        The map() method wraps the underlying sequence in a Stream instance
				.map(testResultToResultRow(testName, suiteName, allTestMethods))
				//toList():- Static method of Collectors class
				// and returns a Collector interface object used to store a group of data onto a list.
				.collect(toList());
	}
	
	
	private Function<ITestResult, String> testResultToResultRow (String testName, String suiteName, List<ITestNGMethod> allTestMethods) {
		return testResult -> {
			String group = null;
			for (ITestNGMethod iTestNGMethod : allTestMethods) {
				if (iTestNGMethod.getMethodName().equals(testResult.getName())) {
					
					group = iTestNGMethod.getGroups()[0];
				}
			}
			switch (testResult.getStatus()) {
				case ITestResult.FAILURE:
					return String.format(ROW_TEMPLATE, "danger", suiteName, testName, testResult.getName(), group, "FAILED", "NA", testResult.wasRetried() ? "Yes" : "No");
				
				case ITestResult.SUCCESS:
					return String.format(ROW_TEMPLATE, "success", suiteName, testName, testResult.getName(), group, "PASSED", String.valueOf(testResult.getEndMillis() - testResult.getStartMillis()), testResult.wasRetried() ? "Yes" : "No");
				
				case ITestResult.SKIP:
					return String.format(ROW_TEMPLATE, "warning", suiteName, testName, testResult.getName(), group, "SKIPPED", "NA", testResult.wasRetried() ? "Yes" : "No");
				
				default:
					return "";
			}
		};
	}
	
	
	private String initReportTemplate () {
		String template = null;
		byte[] reportTemplate;
		try {
			//readAllBytes: Reads all the bytes from a file
			//Paths: Converts a path string, or a sequence of strings that when joined form a path string, to a Path.
			reportTemplate = Files.readAllBytes(Paths.get("src/test/java/resources/reportTemplate.html"));
			//Constructs a new String by decoding the specified array of bytes using the specified charset.
			template = new String(reportTemplate, "UTF-8");
		} catch (IOException e) {
//            LOGGER.error("Problem initializing template", e);
		}
		return template;
	}
	
	
	private void saveReportTemplate (String outputDirectory, String reportTemplate) {
		new File(outputDirectory).mkdirs();
		try {
			//Convenience class for writing character files.
			//FileWriter is meant for writing streams of characters.
			//BufferedWriter: Writes text to a character-output stream, buffering characters so as to provide for the efficient writing of single characters, arrays, and strings.
			
			
			PrintWriter reportWriter = new PrintWriter(new BufferedWriter(new FileWriter(new File(outputDirectory, "CustomisedReport.html"))));
			reportWriter.println(reportTemplate);
			reportWriter.flush();
			reportWriter.close();
		} catch (IOException e) {
//            LOGGER.error("Problem saving template", e);
		}
	}

//    private Function<Collection, ArrayList<ITestNGMethod>> getAllTestMethods() {
//         return collection -> new ArrayList<ITestNGMethod>(collection.stream().collect(Collectors.toList()));
//         }
//
//    }
}
