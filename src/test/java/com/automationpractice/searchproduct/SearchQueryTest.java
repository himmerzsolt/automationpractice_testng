package com.automationpractice.searchproduct;

import com.automationpractice.pageobjects.Results;
import com.automationpractice.pageobjects.components.Button;
import com.automationpractice.pageobjects.components.InputBox;
import com.automationpractice.util.CookieWrite;
import com.automationpractice.util.Grid;
import com.automationpractice.util.RetryAnalyzerImpl;
import com.automationpractice.util.RunEnvironment;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.*;
import org.testng.asserts.SoftAssert;

import java.util.List;

public class SearchQueryTest {
	
	private Results results;
	
	private InputBox searchInputField;
	
	private Button submitSearchButton;
	
	private final SoftAssert softAssert = new SoftAssert();
	
	private final String host = "http://" + Grid.getIP4Address();
	
	
	@DataProvider(name = "existingProducts")
	public Object[][] existingProducts () {
		return new Object[][]{
				{"blouse"},
				{"summer"},
				{"t-shirt"},
		};
	}
	
	
	@DataProvider(name = "nonExistingProducts")
	public Object[][] nonExistingProducts () {
		return new Object[][]{{"winter"}, {"other"}};
	}
	
	
	@Parameters({"os", "browser", "port"})
	@BeforeClass(groups = {"setup"})
	public void setup (String os, String browser, String port) {
		
		WebDriver driver = RunEnvironment.getWebDriver(os, browser, this.host, port);
		results = new Results(driver);
		results.enterFullScreen();
		CookieWrite.writeCookie(driver);
		searchInputField = new InputBox(driver, "#search_query_top");
		submitSearchButton = new Button(driver, "#searchbox > button");
	}
	
	
	@Test(dataProvider = "existingProducts", groups = {"smoke"}, retryAnalyzer= RetryAnalyzerImpl.class)
	public void searchQueryTestForExistingProducts (String searchedQuery) {
		searchInputField.deleteText();
		searchInputField.typeText(searchedQuery);
		submitSearchButton.click();
		
		List<WebElement> searchResults = results.getSearchResults();
		
		for (WebElement searchResult : searchResults) {
			softAssert.assertTrue(searchResult.getAttribute("title").toLowerCase().contains(searchedQuery), searchResult.getAttribute("title") + " does not contain searchedQuery: " + searchedQuery);
		}
		softAssert.assertAll();
	}
	
	
	@Test(dataProvider = "nonExistingProducts", groups = {"regression"}, retryAnalyzer= RetryAnalyzerImpl.class)
	public void searchQueryTestForNonExistingProducts (String searchedQuery) {
		searchInputField.deleteText();
		searchInputField.typeText(searchedQuery);
		submitSearchButton.click();
		
		Assert.assertTrue(results.getContentResultCounterText().contains("0 results have been found."), "There is at least 1 result");
	}
	
	
	@AfterClass(groups = {"setup"})
	public void quitBrowser () {
		results.tearDown();
	}
}
