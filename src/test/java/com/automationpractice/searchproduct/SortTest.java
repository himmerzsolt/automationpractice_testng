package com.automationpractice.searchproduct;

import com.automationpractice.pageobjects.Results;
import com.automationpractice.pageobjects.components.TopMenu;
import com.automationpractice.util.CookieWrite;
import com.automationpractice.util.Grid;
import com.automationpractice.util.RetryAnalyzerImpl;
import com.automationpractice.util.RunEnvironment;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;
import org.testng.asserts.SoftAssert;

import java.util.List;

public class SortTest {
	
	private Results results;
	
	private TopMenu topMenu;
	
	private final SoftAssert softAssert = new SoftAssert();
	
	private final String host = "http://" + Grid.getIP4Address();
	
	
	@DataProvider(name = "getCategories")
	public Object[][] getCategories () {
		Object[][] categories = new Object[2][2];
		
		// 1st row
		categories[0][0] = "Women";
		categories[0][1] = "Tops";
		
		// 2nd row
		categories[1][0] = "Women";
		categories[1][1] = "Dresses";
		
		return categories;
	}
	
	
	@Parameters({"os", "browser", "port"})
	@BeforeClass(groups = {"setup"})
	public void setup (String os, String browser, String port) {
		
		WebDriver driver = RunEnvironment.getWebDriver(os, browser, this.host, port);
		results = new Results(driver);
		results.enterFullScreen();
		topMenu = new TopMenu(driver);
		CookieWrite.writeCookie(driver);
	}
	
	
	@Test(dataProvider = "getCategories", groups = {"smoke"}, retryAnalyzer = RetryAnalyzerImpl.class)
	public void sortProductsAscendingByPrice (String category, String subCategory) {
		List<Double> sortedPrices;
		List<Double> actualPrices;
		
		topMenu.selectCategory(category);
		topMenu.selectSubCategory(subCategory);
		sortedPrices = results.sortSearchResultPricesAscending();
		results.clickOnSelectProductSortByPriceAsc();
		actualPrices = results.getSearchResultPrices();
		
		
		for (int i = 0; i < actualPrices.size(); i++) {
			softAssert.assertTrue(actualPrices.get(i).equals(sortedPrices.get(i)));
			softAssert.assertAll();
		}
	}
	
	
	@AfterClass(groups = {"setup"})
	public void quitBrowser () {
		results.tearDown();
	}
}
