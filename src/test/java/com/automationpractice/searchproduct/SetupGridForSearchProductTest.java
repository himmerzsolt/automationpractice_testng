package com.automationpractice.searchproduct;

import com.automationpractice.util.Grid;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
public class SetupGridForSearchProductTest {
	
	private final Grid grid = new Grid();
	
	
	@BeforeSuite(groups = {"setup"})
	public void startHub () {
		grid.startHub();
	}
	
	
	@AfterSuite(groups = {"setup"})
	public void stopGrid () {
		grid.stopGrid();
	}
}
