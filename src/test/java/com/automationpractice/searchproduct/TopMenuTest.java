package com.automationpractice.searchproduct;

import com.automationpractice.pageobjects.Results;
import com.automationpractice.pageobjects.components.TopMenu;
import com.automationpractice.util.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.*;
import org.testng.asserts.SoftAssert;

import java.util.List;

public class TopMenuTest {
	
	private Results results;
	
	private TopMenu topMenu;
	
	private final SoftAssert softAssert = new SoftAssert();
	
	private final String host = "http://" + Grid.getIP4Address();
	
	
	@DataProvider(name = "getCategories")
	public Object[][] getCategories () {
		Object[][] categories = ExcelReader.getCategories();
		
		
		return categories;
	}
	
	
	@Parameters({"os", "browser", "port"})
	@BeforeClass(groups = {"setup"})
	public void setup (String os, String browser, String port) {
		
		WebDriver driver = RunEnvironment.getWebDriver(os, browser, host, port);
		CookieWrite.writeCookie(driver);
		results = new Results(driver);
		results.enterFullScreen();
		topMenu = new TopMenu(driver);
	}
	
	
	@Test(dataProvider = "getCategories", groups = {"smoke"}, retryAnalyzer = RetryAnalyzerImpl.class)
	public void searchProductsInTopMenu (String category, String subCategory) {
		topMenu.selectCategory(category);
		topMenu.selectSubCategory(subCategory);
		List<WebElement> searchResults = results.getSearchResults();
		for (WebElement searchResult : searchResults) {
			softAssert.assertTrue(searchResult.getAttribute("title").toLowerCase().contains(subCategory),
					searchResult.getAttribute("title") + " does not contain subcategory: " + subCategory);
		}
		softAssert.assertAll();
	}
	
	
	@AfterClass(groups = {"setup"})
	public void quitBrowser () {
		results.tearDown();
	}
}
